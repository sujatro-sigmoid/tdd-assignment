import os
import pytest
from number_of_unique_chars import UniqueCharacterFinder
from unittest.mock import MagicMock


class Test_Assignment:
    @pytest.fixture
    def unique_chars_fixture(self):
        return UniqueCharacterFinder()

    @classmethod
    def setup_class(cls):
        print("Setup class for no of unique chars.")

    @classmethod
    def teardown_class(cls):
        print("Teardown class for no of unique chars.")

    def test_input_file(self):
        assert os.path.exists("input_file") == 1

    @pytest.mark.parametrize("test_input,expected", [("asa", 2), ("abcd", 4), ("aaa", 1)])
    def test_parameterized(self, unique_chars_fixture, test_input, expected):
        assert unique_chars_fixture.findUniqueCharacters(test_input) == expected

    def test_negative_tests(self, unique_chars_fixture):
        with pytest.raises(Exception):
            unique_chars_fixture.findUniqueCharacters(-101)
        with pytest.raises(Exception):
            unique_chars_fixture.findUniqueCharacters(2.3)

    def test_correct_out(self, unique_chars_fixture, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value="1")
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        result = unique_chars_fixture.readFromFile("input_file")
        mock_open.assert_called_once_with("input_file", "r")
        assert result == "1"

